import time
import pandas as pd
from selenium import webdriver

driver = webdriver.Chrome(executable_path ='D:\selenium-driver\chromedriver.exe')  # Optional argument, if not specified will search path.

def get_vales(prof_url,df):
    company_name, profile_url, company_website_url,location ,tags, founding_date, founders,employee_range, urls, emails, phones, description_short ,description = '','','','','','','','','','','','',''
    company_name = driver.find_element_by_class_name("startup_name").text
    profile_url = prof_url
    company_website_url = driver.find_element_by_class_name("startup_website").get_attribute('href')
    location = driver.find_element_by_class_name("startup_location").text
    tags = driver.find_element_by_class_name("startup_market").text
 
    month = driver.find_element_by_class_name("startup_found_month").text
    year = driver.find_element_by_class_name("startup_found_year").text
    founding_date =''
    if month and year not in ['', ' ']:
        founding_date = parse_date(month, year)
    
    founders = driver.find_element_by_css_selector('.team').text
    employee_range = ''
    web_el_urls = driver.find_elements_by_xpath("//*[@id='startupView']/div[1]/div/div[1]/div/div/div/div/div/div[2]/div[2]/div/div/div/div/a")
    urls = []
    for url in web_el_urls:
        urls.append(url.get_attribute('href'))
    urls = ', '.join(urls)
    emails = ''
    phones = ''
    description_short = driver.find_element_by_class_name("startup_short_description").text
    description = driver.find_element_by_class_name("portlet-body").text
    df = df.append({'company_website_url':company_website_url,'location': location,'tags': tags, 'founding_date': founding_date, 'founders': founders,'employee_range': employee_range, 'urls' : urls, 'emails': emails, 'phones': phones, 'description_short': description_short ,'description': description}, ignore_index=True)
    return df

def parse_date(m,y):
    date = ''
    month = time.strptime(m,'%B').tm_mon
    if month <10:
        date = y + '-0' + str(month) + '-01'
    else:
        date = y + '-' + str(month) + '-01'
    return date
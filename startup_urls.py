# -*- coding: utf-8 -*-
import pandas as pd
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

driver = webdriver.Firefox(executable_path='D:\selenium-driver\geckodriver.exe')
driver.get('https://e27.co/startups/')
while True:
    try:
        loadMoreButton = driver.find_element_by_xpath("//button[contains(.,'Load More')]")
        from selenium.webdriver.support import expected_conditions as EC
        wait = WebDriverWait(driver, 50)
        element = wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Load More')]")))
        loadMoreButton.click()
    except Exception as e:
        print(e)
        break
print("Complete")
items = driver.find_elements_by_class_name("startuplink") 
links = []
for item in items:
    href = item.get_attribute('href')
    links.append(href)

links_df = pd.DataFrame(links)
links_df.to_csv('startuplinks.csv', index=False, header=['startuplink'])

driver.close()
from utils import *
data = pd.read_csv('startuplinks.csv')
data = data.dropna()
random_subset = data.sample(n=250)

df = pd.DataFrame(columns=['company_website_url','location' ,'tags', 'founding_date', 'founders','employee_range', 'urls', 'emails', 'phones', 'description_short' ,'description'])
for prof_url in random_subset['startuplink']:
    try:
        driver.get(prof_url)
        time.sleep(2)
        df = get_vales(prof_url, df)
        time.sleep(3)
    except:
        driver.close()
        driver = webdriver.Chrome(executable_path ='D:\selenium-driver\chromedriver.exe')  # Optional argument, if not specified will search path.
        driver.get(prof_url)
        time.sleep(3)
        df = get_vales(prof_url, df)
        time.sleep(3)
    print(prof_url)
driver.close()
df.to_excel('startup_desc.xls', index= False)